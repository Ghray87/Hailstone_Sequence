Hilestone Sequence
--------------------------------------------------------------------
Author: Raimondo Pastore
*********************************************************************

The Collatz conjecture is a conjecture in mathematics that concerns a sequence defined as follows: start with any positive integer n. 
Then each term is obtained from the previous term as follows: 
if the previous term is even, the next term is one half the previous term. 
Otherwise, the next term is 3 times the previous term plus 1. 
The conjecture is that no matter what value of n, the sequence will always reach 1.

The program has the goal to generate an Hailstone Sequence and
an HTML Page with some information about the insert number.

The project is created with the use of Maven and Eclipse.

You can find the source code in the folder src.

At the path "target/Hailstone-Sequence.jar", you can find the executable jar of the application with a minimal GUI interface.

In the folder "examples" there are some examples of the output.
